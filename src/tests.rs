use crate::models::{Feed, StreamType};
use crate::GReaderApi;
use reqwest::Client;
use std::env;
use url::Url;

fn test_setup_env() -> GReaderApi {
    dotenv::dotenv().expect("Failed to read .env file");
    let url = env::var("GREADER_URL").expect("Failed to read GREADER_URL");
    let user = env::var("GREADER_USER").expect("Failed to read GREADER_USER");
    let pw = env::var("GREADER_PW").expect("Failed to read GREADER_PW");

    let url = Url::parse(&url).unwrap();
    GReaderApi::new(&url, &user, &pw)
}

async fn setup_env_get() -> GReaderApi {
    let mut api = test_setup_env();
    let _response = api.login(&Client::new()).await;
    api
}

async fn setup_env_post() -> GReaderApi {
    let mut api = test_setup_env();
    let _response = api.login(&Client::new()).await;
    let _response = api.token(&Client::new()).await;
    api
}

#[tokio::test]
async fn auth() {
    let mut api = test_setup_env();
    let response = api.login(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn token() {
    let mut api = setup_env_get().await;
    let response = api.token(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn user_info() {
    let api = setup_env_get().await;
    let response = api.user_info(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn unread_count() {
    let api = setup_env_get().await;
    let response = api.unread_count(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn subscription_list() {
    let api = setup_env_get().await;
    let response = api.subscription_list(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn subscription_create_edit_delete() {
    let api = setup_env_post().await;

    let sample_feed = Url::parse("https://feedforall.com/sample.xml").unwrap();
    let feed_name = "TESTING";

    let _response = api.subscription_delete(&sample_feed, &Client::new()).await;

    let response = api
        .subscription_create(&sample_feed, feed_name, None, &Client::new())
        .await;
    assert!(response.is_ok());

    let response = api.subscription_list(&Client::new()).await;
    assert!(response.is_ok());
    let mut current_feed: Feed = Feed {
        id: "placeholder".to_string(),
        title: "placeholder".to_string(),
        categories: Vec::new(),
        url: "placeholder".to_string(),
        html_url: "placeholder".to_string(),
        icon_url: "placeholder".to_string(),
    };

    for feed in response.unwrap().subscriptions {
        if feed.title == feed_name {
            current_feed = feed;
        }
    }

    let response = api
        .subscription_edit(
            &sample_feed,
            "AN EXAMPLE",
            &current_feed.categories[0].id,
            &current_feed.categories[0].id,
            &Client::new(),
        )
        .await;
    assert!(response.is_ok());

    let response = api.subscription_delete(&sample_feed, &Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn subscription_quickadd() {
    let api = setup_env_post().await;
    let sample_feed = Url::parse("https://feedforall.com/sample-feed.xml").unwrap();

    let _response = api.subscription_delete(&sample_feed, &Client::new()).await;

    let response = api
        .subscription_quickadd(&sample_feed, &Client::new())
        .await;
    assert!(response.is_ok());

    let response = api.subscription_delete(&sample_feed, &Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn stream_contents() {
    let api = setup_env_post().await;

    let response = api
        .stream_contents(
            "feed/1",
            false,
            None,
            None,
            None,
            None,
            None,
            None,
            &Client::new(),
        )
        .await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn item_ids() {
    let api = setup_env_post().await;

    let response = api
        .items_ids(
            "feed/1",
            50,
            false,
            None,
            None,
            None,
            None,
            None,
            &Client::new(),
        )
        .await;
    assert!(response.is_ok());
}

#[cfg(feature = "feedhq")]
#[tokio::test]
async fn items_count() {
    let api = setup_env_post().await;

    let response = api.items_count("feed/1", false, &Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn items_contents() {
    let api = setup_env_post().await;

    let response = api
        .items_contents(
            vec![
                "1591997825171874".to_string(),
                "1591997825171871".to_string(),
            ],
            &Client::new(),
        )
        .await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn tag_list() {
    let api = setup_env_get().await;
    let response = api.tag_list(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn tag_edit_rename_delete() {
    let tag_name_1 = "user/-/label/com.google/test";
    let tag_name_2 = "user/-/label/com.google/works";
    let item_id = "1591997825171867";
    let api = setup_env_post().await;

    // remove tage from articles to have start consistent
    let _response = api
        .tag_delete(StreamType::Stream, tag_name_1, &Client::new())
        .await;
    let _response = api
        .tag_delete(StreamType::Stream, tag_name_2, &Client::new())
        .await;

    let response = api.tag_list(&Client::new()).await;
    assert!(response.is_ok());
    let tag_amount_old = response.unwrap().tags.len();

    // adding new label
    let response = api
        .tag_edit(vec![(item_id, Some(tag_name_1), None)], &Client::new())
        .await;
    assert!(response.is_ok());

    let response = api.tag_list(&Client::new()).await;
    assert!(response.is_ok());
    let tag_amount_new = response.unwrap().tags.len();
    assert_eq!(tag_amount_old + 1, tag_amount_new);

    // renaming should not change anything
    let response = api
        .tag_rename(StreamType::Stream, tag_name_1, tag_name_2, &Client::new())
        .await;
    assert!(response.is_ok());

    let response = api.tag_list(&Client::new()).await;
    assert!(response.is_ok());
    let tag_amount_new = response.unwrap().tags.len();
    assert_eq!(tag_amount_old + 1, tag_amount_new);

    // delete tag
    let response = api
        .tag_delete(StreamType::Stream, tag_name_2, &Client::new())
        .await;
    assert!(response.is_ok());

    let response = api.tag_list(&Client::new()).await;
    assert!(response.is_ok());
    let tag_amount_new = response.unwrap().tags.len();
    assert_eq!(tag_amount_old, tag_amount_new);
}

#[tokio::test]
async fn mark_all_as_read() {
    let api = setup_env_post().await;
    let response = api
        .mark_all_as_read("feed/1", Some(1564444800), &Client::new())
        .await;
    assert!(response.is_ok());
}
