use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Unread {
    pub max: u64,
    pub unreadcounts: Vec<UnreadFeed>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UnreadFeed {
    pub count: u64,
    pub id: String,
    pub newest_item_timestamp_usec: String,
}
