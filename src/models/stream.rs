use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Summary {
    pub content: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Alternate {
    pub href: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Origin {
    pub stream_id: String,
    pub title: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Item {
    pub origin: Option<Origin>,
    pub updated: Option<i64>,
    pub id: String,
    pub categories: Vec<String>,
    pub author: String,
    pub alternate: Vec<Alternate>,
    pub timestamp_usec: String,
    pub crawl_time_msec: String,
    pub published: i64,
    pub title: String,
    #[serde(alias = "summary")]
    pub content: Summary,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Stream {
    pub direction: Option<String>,
    pub id: String,
    pub title: Option<String>,
    pub description: Option<String>,
    // r#self (raw identifier) could not be used, therefore i gets renamed to own
    #[serde(rename = "self")]
    pub own: Option<Vec<(String, String)>>,
    pub updated: i64,
    pub updated_usec: Option<String>,
    pub items: Vec<Item>,
    pub author: Option<String>,
    pub continuation: Option<String>,
}
