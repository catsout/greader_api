use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct GReaderError {
    pub errors: Vec<String>,
}

impl PartialEq for GReaderError {
    fn eq(&self, other: &GReaderError) -> bool {
        self.errors == other.errors
    }
}

impl Eq for GReaderError {}
