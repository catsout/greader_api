use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Category {
    pub id: String,
    pub label: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Feed {
    pub id: String,
    pub title: String,
    pub categories: Vec<Category>,
    pub url: String,
    pub html_url: String,
    pub icon_url: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Feeds {
    pub subscriptions: Vec<Feed>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct QuickFeed {
    pub num_results: u64,
    pub query: String,
    pub stream_id: String,
}

impl Category {
    pub fn decompose(self) -> (String, String) {
        (self.id, self.label)
    }
}

impl Feed {
    pub fn decompose(self) -> (String, String, Vec<Category>, String, String, String) {
        (
            self.id,
            self.title,
            self.categories,
            self.url,
            self.html_url,
            self.icon_url,
        )
    }
}
