use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct ItemIds {
    pub id: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ItemRefs {
    pub item_refs: Vec<ItemIds>,
}
