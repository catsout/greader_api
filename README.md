# greader_api &emsp; [![build]][pipeline] [![Latest Version]][crates.io] [![Rustc Version 1.31+]][rustc]

[Latest Version]: https://img.shields.io/crates/v/greader_api.svg
[crates.io]: https://crates.io/crates/greader_api
[build]: https://img.shields.io/gitlab/pipeline/news-flash/greader_api
[pipeline]: https://gitlab.com/news-flash/greader_api/-/pipelines
[Rustc Version 1.31+]: https://img.shields.io/badge/rustc-1.31+-lightgray.svg
[rustc]: https://blog.rust-lang.org/2018/12/06/Rust-1.31-and-rust-2018.html

possible endpoints:
- Fresh RSS
- Feed HQ (untested)
- Inno Reader (untested)
- The Old Reader (untested)
